package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.config.SecurityService;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserCreateDto;
import by.itstep.collectionsphotos.dto.userDto.UserFullDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.mapper.UserMapper;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    SecurityService securityService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CollectionMapper collectionMapper;

    public UserFullDto findById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        UserEntity user = userRepository.findById(id);

        if(!authorizedUser.getId().equals(user.getId())){
            throw new RuntimeException("UserService -> You are not authorized");
        }
        if (user == null) {
            throw new RuntimeException("User not found by id: " + id);
        }
        System.out.println("UserService -> Found user (by id: " + id + "): " + user);
        UserFullDto userFullDto = userMapper.map(user);

        List<CollectionShortDto> collectionShortDtos = collectionMapper.map(user.getCollections());
        userFullDto.setCollections(collectionShortDtos);

        return userFullDto;
    }

    public List<UserShortDto> findAll() {
        List<UserEntity> allUserEntities = userRepository.findAll();
        System.out.println("UserService -> Found users: " + allUserEntities);
        List<UserShortDto> allDtos = userMapper.map(allUserEntities);
        return allDtos;
    }

    public UserFullDto findByEmail(String email) {
        UserEntity user = userRepository.findByEmail(email);
        if (user == null) {
            throw new RuntimeException("User not found by email: " + email);
        }
        System.out.println("UserService -> Found user (by email: " + email + "): " + user);

        UserFullDto userFullDto = userMapper.map(user);
        return userFullDto;
    }

    public UserFullDto create(UserCreateDto createRequest) {
        UserEntity user = userMapper.map(createRequest);
        if (user.getId() != null) {
            throw new RuntimeException("UserService - > Can't create which already has id");
        }
        if (userRepository.findByEmail(user.getEmail()) != null) {
            throw new RuntimeException("Email: " + user.getEmail() + " is taken");
        }
        UserEntity createdUser = userRepository.create(user);
        UserFullDto userFullDto = userMapper.map(createdUser);
        System.out.println("UserService -> Created user: " + userFullDto);
        return userFullDto;
    }

    public UserFullDto update(UserUpdateDto updateRequest) {

        UserEntity currentUser = securityService.getAuthenticateUser();
        if (!currentUser.getId().equals(updateRequest.getId())) {
            throw new RuntimeException("UserService ->  Can't update another user!");
        }

        UserEntity user = userRepository.findById(updateRequest.getId());
        if (user == null) {
            throw new RuntimeException("UserService - > User not found by id: " + updateRequest.getId());
        }

        user.setName(updateRequest.getName());
        user.setLogin(updateRequest.getLogin());

        UserEntity updatedUser = userRepository.update(user);
        System.out.println("UserService -> Updated user: " + updatedUser);
        UserFullDto updatedDto = userMapper.map(updatedUser);

        return updatedDto;
    }

    public void deleteById(int id) {
        UserEntity userToDelete = userRepository.findById(id);

        if (userToDelete == null) {
            throw new RuntimeException("User was not found by id: " + id);
        }
        List<CollectionEntity> existingCollections = userToDelete.getCollections();
        List<CommentEntity> existingComments = userToDelete.getComments();

        for (CommentEntity comment : existingComments) {
            commentRepository.deleteById(comment.getId());
        }

        for (CollectionEntity collection : existingCollections) {
            CollectionEntity foundCollection = collectionRepository.findById(collection.getId());
            List<PhotoEntity> existingPhotos = foundCollection.getPhotos();
            collectionRepository.deleteById(collection.getId());
            for (PhotoEntity photo : existingPhotos) {
                photoRepository.deleteById(photo.getId());
            }
        }
        userRepository.deleteById(id);
        System.out.println("UserService -> Delete user: " + userToDelete);
    }
}
