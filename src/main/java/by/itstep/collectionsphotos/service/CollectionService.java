package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.collectionDto.*;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionMapper collectionMapper;

    public CollectionFullDto findById(int id) {
        CollectionEntity collectionEntity = collectionRepository.findById(id);
        if (collectionEntity == null) {
            throw new RuntimeException("Collection not found by id: " + id);
        }
        System.out.println("CollectionService -> Found collection (by id: " + id + "): " + collectionEntity);
        CollectionFullDto collectionFullDto = collectionMapper.map(collectionEntity);
        return collectionFullDto;
    }

    public List<CollectionShortDto> findAll() {

        List<CollectionEntity> allCollection = collectionRepository.findAll();
        System.out.println("CollectionService -> Found collections: " + allCollection);
        List<CollectionShortDto> collectionShortDtos = collectionMapper.map(allCollection);
        return collectionShortDtos;
    }

    public CollectionFullDto create(CollectionCreateDto collectionCreateDto){

        if(collectionCreateDto.getUserId() == null){
            throw new RuntimeException("CollectionService - > Can't create a collection that doesn't have a user");
        }
        UserEntity userEntity = userRepository.findById(collectionCreateDto.getUserId());

        if(userEntity == null){ // ololo
            throw new RuntimeException("CollectionService -> we cannot create a collection," +
                    " it does not have a user from the database");
        }

        CollectionEntity collectionEntity = collectionMapper.map(collectionCreateDto);
        collectionEntity.setUser(userEntity);

        CollectionEntity createdCollectionEntity = collectionRepository.create(collectionEntity);

        System.out.println("CollectionService -> Created collection: " + createdCollectionEntity + " for the user ->"
                + userEntity);
        CollectionFullDto collectionFullDto = collectionMapper.map(createdCollectionEntity);
        return collectionFullDto;
    }

    public CollectionFullDto update(CollectionUpdateDto collectionUpdateDto){
        if (collectionUpdateDto.getId() == null) {
            throw new RuntimeException("CollectionService - > Can't updated entity without id");
        }
        CollectionEntity collectionEntity = collectionRepository.findById(collectionUpdateDto.getId());

        if (collectionEntity == null) {
            throw new RuntimeException("CollectionService - > Collection not found by id: " + collectionUpdateDto.getId());
        }
        UserEntity user = collectionEntity.getUser();
        if(user == null){
            throw new RuntimeException("CollectionService -> we cannot update a collection," +
                    " it does not have a user from the database");
        }
        collectionEntity.setName(collectionUpdateDto.getName());
        collectionEntity.setDescription(collectionUpdateDto.getDescription());

        CollectionEntity updatedCollection = collectionRepository.update(collectionEntity);
        System.out.println("CollectionService -> Updated collection: " + updatedCollection + " for the user ->" + user);

        CollectionFullDto collectionFullDto = collectionMapper.map(updatedCollection);
        return collectionFullDto;
    }

    public void deleteById(int id) {
        CollectionEntity collectionToDelete = collectionRepository.findById(id);
        if (collectionToDelete == null) {
            throw new RuntimeException("Collection was not found by id: " + id);
        }
        collectionRepository.deleteById(id);
        System.out.println("CollectionService -> Deleted collection: " + collectionToDelete);
    }

    public CollectionFullDto addPhotoToCollection(CollectionCreatePhotoInCollectionDto collectionCreatePhotoInCollectionDto){

        CollectionEntity collectionEntity = collectionRepository.findById(collectionCreatePhotoInCollectionDto.getCollectionId());
        if(collectionEntity == null){
            throw  new RuntimeException("CollectionService -> this collection: " + collectionEntity + " is not in the database");
        }
        List<PhotoEntity> photosInCollection = collectionEntity.getPhotos();
        PhotoEntity photoEntity = photoRepository.findById(collectionCreatePhotoInCollectionDto.getPhotoId());

        if(photoEntity == null){
            throw  new RuntimeException("CollectionService -> this photo: " + photoEntity + " is not in the database");
        }
        if(photosInCollection.contains(photoEntity)){
            throw  new RuntimeException("CollectionService -> photo: " + photoEntity
                    + " is already in collection: " + collectionEntity);
        }
        photosInCollection.add(photoEntity);
        collectionEntity.setPhotos(photosInCollection);
        collectionRepository.update(collectionEntity);
        System.out.println("CollectionService -> photo: " + photoEntity + "was added to collection: " + collectionEntity);
        CollectionFullDto collectionFullDto = collectionMapper.map(collectionEntity);
        return collectionFullDto;
    }

    public CollectionFullDto removePhotoFromCollection(CollectionRemovePhotoInCollectionDto collectionRemovePhotoInCollectionDto){

        CollectionEntity collectionEntity = collectionRepository.findById(collectionRemovePhotoInCollectionDto.getCollectionId());
        if(collectionEntity == null){
            throw  new RuntimeException("CollectionService -> this collection: " + collectionEntity + " is not in the database");
        }
        List<PhotoEntity> photosInCollection = collectionEntity.getPhotos();
        PhotoEntity photoEntity = photoRepository.findById(collectionRemovePhotoInCollectionDto.getPhotoId());

        if(photoEntity == null){
            throw  new RuntimeException("CollectionService -> this photo: " + photoEntity + " is not in the database");
        }
        if(!photosInCollection.contains(photoEntity)){
            throw  new RuntimeException("CollectionService -> photo: " + photoEntity
                    + " not in the collection: " + collectionEntity);
        }
        photosInCollection.remove(photoEntity);
        collectionEntity.setPhotos(photosInCollection);
        collectionRepository.update(collectionEntity);

        System.out.println("CollectionService -> photo: " + photoEntity + "has been removed from collection: " + collectionEntity);
        CollectionFullDto collectionFullDto = collectionMapper.map(collectionEntity);
        return collectionFullDto;
    }




}
