package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.photoDto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {


    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    PhotoMapper photoMapper;

    public PhotoFullDto findById(int id) {
        PhotoEntity photo = photoRepository.findById(id);
        if (photo == null) {
            throw new RuntimeException("Photo not found by id: " + id);
        }
        System.out.println("PhotoService -> Found photo (by id: " + id + "): " + photo);
        PhotoFullDto photoFullDto = photoMapper.map(photo);
        return photoFullDto;
    }

    public List<PhotoShortDto> findAll(){
        List<PhotoEntity> allPhotos = photoRepository.findAll();
        System.out.println("PhotoService -> Found photos: " + allPhotos);
        List<PhotoShortDto> photoShortDtos = photoMapper.map(allPhotos);
        return photoShortDtos;
    }

    public PhotoFullDto create(PhotoCreateDto photoCreateDto) {
        PhotoEntity photoEntity = photoMapper.map(photoCreateDto);
        if (photoEntity.getId() != null) {
            throw new RuntimeException("PhotoService - > Can't create which already has id");
        }
        photoEntity.setRating(0);
        PhotoEntity createdPhoto = photoRepository.create(photoEntity);
        PhotoFullDto photoFullDto = photoMapper.map(createdPhoto);

        System.out.println("PhotoService -> Created photo: " + createdPhoto);
        return photoFullDto;
    }

    public PhotoFullDto update(PhotoUpdateDto photoUpdateDto) {

        if (photoUpdateDto.getId() == null) {
            throw new RuntimeException("PhotoService - > Can't updated entity without id");
        }
        PhotoEntity photoEntity = photoRepository.findById(photoUpdateDto.getId());
        if (photoEntity == null) {
            throw new RuntimeException("PhotoService - > Photo not found by id: " + photoUpdateDto.getId());
        }
        photoEntity.setName(photoUpdateDto.getName());

        PhotoEntity updatedPhotoEntity = photoRepository.update(photoEntity);
        System.out.println("PhotoService -> Updated photo: " + updatedPhotoEntity);
        PhotoFullDto photoFullDto = photoMapper.map(updatedPhotoEntity);
        return photoFullDto;
    }

    public void deleteById(int id) {

        PhotoEntity photoToDelete = photoRepository.findById(id);

        if (photoToDelete == null) {
            throw new RuntimeException("Photo was not found by id: " + id);
        }
        List<CollectionEntity> updateCollections = photoToDelete.getCollections();

        for(CollectionEntity collection : updateCollections){
            collection.getPhotos().remove(photoToDelete);
            collectionRepository.update(collection);
        }

        List<CommentEntity> existingComments = photoToDelete.getComments();

        for (CommentEntity comment : existingComments) {
            commentRepository.deleteById(comment.getId());
        }
        photoRepository.deleteById(id);
        System.out.println("PhotoService -> Delete photo: " + photoToDelete);
    }

}
