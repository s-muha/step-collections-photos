package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.commentDto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentMapper commentMapper;

    public CommentFullDto findById(int id) {
        CommentEntity commentEntity = commentRepository.findById(id);
        if (commentEntity == null) {
            throw new RuntimeException("Comment not found by id: " + id);
        }
        CommentFullDto commentFullDto = commentMapper.map(commentEntity);
        System.out.println("CommentService -> Found comment (by id: " + id + "): " + commentFullDto);
        return commentFullDto;
    }

    public List<CommentShortDto> findAll() {

        List<CommentEntity> allCommentEntities = commentRepository.findAll();
        List<CommentShortDto> allCommentShortDtos = commentMapper.map(allCommentEntities);
        System.out.println("CommentService -> Found comments: " + allCommentShortDtos);
        return allCommentShortDtos;
    }

    public CommentFullDto create(CommentCreateDto commentCreateDto) {
        if(commentCreateDto.getMessage() == null) {
            throw new RuntimeException("CommentService -> Can't create empty message");
        }
        if(commentCreateDto.getUserId() == null){
            throw new RuntimeException("CommentService ->  comment cannot be created without user");
        }
        if(commentCreateDto.getPhotoId() == null){
            throw new RuntimeException("CommentService ->  Can't post a comment without a photo");
        }
        UserEntity user = userRepository.findById(commentCreateDto.getUserId());
        if(user == null){
            throw new RuntimeException("CommentService -> we cannot create a comment," +
                    " it does not have a user from the database");
        }
        PhotoEntity photo = photoRepository.findById(commentCreateDto.getPhotoId());
        if(photo == null){
            throw new RuntimeException("CommentService -> we cannot create a comment," +
                    " it does not have a photo from the database");
        }
        CommentEntity commentEntity = commentMapper.map(commentCreateDto);
        commentEntity.setUser(user);
        commentEntity.setPhoto(photo);
        CommentEntity createdCommentEntity = commentRepository.create(commentEntity);
        System.out.println("CommentService -> Created comment: \"" + createdCommentEntity + "\" from user: " +
                user + " for the photo ->" + photo);
        CommentFullDto commentFullDto = commentMapper.map(createdCommentEntity);
        return commentFullDto;
    }

    public CommentFullDto update(CommentUpdateDto commentUpdateDto) {

        if (commentUpdateDto.getId() == null) {
            throw new RuntimeException("CommentService - > Can't updated entity without id");
        }
        if(commentUpdateDto.getMessage() == null){
            throw new RuntimeException("CommentService - > Can't updated entity without description");
        }
        CommentEntity commentEntityFromDb = commentRepository.findById(commentUpdateDto.getId());
        if (commentEntityFromDb == null) {
            throw new RuntimeException("CommentService - > Comment not found by id: " + commentUpdateDto.getId());
        }
        UserEntity userEntity = commentEntityFromDb.getUser();
        if(userEntity == null){
            throw new RuntimeException("CommentService - > we cannot update a comment it does not have a user");
        }
        if(userEntity.getId() == null){
            throw new RuntimeException("CommentService -> we can't update the comment, it doesn't have a user with id");
        }
        if(userRepository.findById(userEntity.getId()) == null){
            throw new RuntimeException("CommentService -> we cannot update a comment," +
                    " it does not have a user from the database");
        }
        PhotoEntity photoEntity = commentEntityFromDb.getPhoto();
        if(photoEntity == null){
            throw new RuntimeException("CommentService - > we cannot update a comment it does not have a photo");
        }
        if(photoEntity.getId() == null){
            throw new RuntimeException("CommentService -> we can't update the comment, it doesn't have a photo with id");
        }
        if(photoRepository.findById(photoEntity.getId()) == null){
            throw new RuntimeException("CommentService -> we cannot update a comment," +
                    " it does not have a photo from the database");
        }
        UserEntity userEntityFromDb = userRepository.findById(userEntity.getId());
        PhotoEntity photoEntityFromDb = photoRepository.findById(photoEntity.getId());

        commentEntityFromDb.setMessage(commentUpdateDto.getMessage());
        commentRepository.update(commentEntityFromDb);

        System.out.println("CommentService -> Updated comment: \"" + commentEntityFromDb + "\" user: " +
                commentEntityFromDb + " for the photo ->" + photoEntityFromDb);
        CommentFullDto commentFullDto = commentMapper.map(commentEntityFromDb);
        return commentFullDto;
    }

    public void deleteById(int id) {
        CommentEntity commentToDelete = commentRepository.findById(id);
        if (commentToDelete == null) {
            throw new RuntimeException("Comment was not found by id: " + id);
        }
        commentRepository.deleteById(id);
        System.out.println("CommentService -> Delete comment: " + commentToDelete);
    }

}
