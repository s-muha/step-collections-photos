package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.userDto.UserCreateDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserFullDto;
import by.itstep.collectionsphotos.dto.userDto.UserUpdateDto;
import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    UserEntity map(UserCreateDto userCreateDto);

    default List<UserShortDto> map(List<UserEntity> userEntities){
        List<UserShortDto> userShortDtos = new ArrayList<>();
        for(UserEntity userEntity : userEntities){
            userShortDtos.add(mapOneUserInShortDto(userEntity));
        }
        return userShortDtos;
    }

    UserShortDto mapOneUserInShortDto(UserEntity userEntity);

    UserCreateDto mapUserCreateDto(UserEntity userEntity);

    UserUpdateDto mapUserUpdateDto(UserEntity userEntity);

}




