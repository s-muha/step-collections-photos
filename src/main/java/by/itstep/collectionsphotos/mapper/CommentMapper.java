package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.commentDto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import lombok.Data;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CommentMapper {

    CommentFullDto map(CommentEntity commentEntity);

    CommentEntity map(CommentCreateDto commentCreateDto);

    default List<CommentShortDto> map(List<CommentEntity> commentEntities){
        List<CommentShortDto> commentShortDtos = new ArrayList<>();
        for(CommentEntity commentEntity : commentEntities){
            commentShortDtos.add(mapOneCommentInShortDto(commentEntity));
        }
        return commentShortDtos;
    }

    CommentShortDto mapOneCommentInShortDto(CommentEntity commentEntity);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "photoId", source = "photo.id")
    CommentCreateDto mapCommentCreateDto(CommentEntity commentEntity);

    CommentUpdateDto mapCommentUpdateDto(CommentEntity commentEntity);

}




