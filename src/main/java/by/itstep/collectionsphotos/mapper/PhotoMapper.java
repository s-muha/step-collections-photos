package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.photoDto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import lombok.Data;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface PhotoMapper {
    
    PhotoFullDto map(PhotoEntity photoEntity);

    default List<PhotoShortDto> map(List<PhotoEntity> photoEntities){
        List<PhotoShortDto> photoShortDtos = new ArrayList<>();
        for(PhotoEntity photoEntity : photoEntities){
            photoShortDtos.add(mapOnePhotoInShortDto(photoEntity));
        }
        return photoShortDtos;
    }

    PhotoEntity map(PhotoCreateDto photoCreateDto);

    PhotoShortDto mapOnePhotoInShortDto(PhotoEntity photoEntity);

    PhotoCreateDto mapPhotoCreateDto(PhotoEntity photoEntity);

    PhotoUpdateDto mapPhotoUpdateDto(PhotoEntity photoEntity);

}
