package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.collectionDto.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionFullDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionUpdateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import lombok.Data;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CollectionMapper {

    CollectionFullDto map(CollectionEntity collectionEntity);

    default List<CollectionShortDto> map(List<CollectionEntity> collectionEntities) {
        List<CollectionShortDto> collectionShortDtos = new ArrayList<>();

        for(CollectionEntity collectionEntity : collectionEntities){
            CollectionShortDto collectionShortDto = new CollectionShortDto();
            collectionShortDto.setId(collectionEntity.getId());
            collectionShortDto.setName(collectionEntity.getName());
            collectionShortDto.setDescription(collectionEntity.getDescription());

            collectionShortDtos.add(collectionShortDto);
        }
        return collectionShortDtos;
    }

    CollectionEntity map(CollectionCreateDto collectionCreateDto);

    @Mapping(target = "userId", source = "user.id")
    CollectionCreateDto mapCollectionCreateDto(CollectionEntity collectionEntity);

    CollectionUpdateDto mapCollectionUpdateDto(CollectionEntity collectionEntity);

}
