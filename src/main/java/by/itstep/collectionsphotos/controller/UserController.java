package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.userDto.UserCreateDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserFullDto;
import by.itstep.collectionsphotos.dto.userDto.UserUpdateDto;
import by.itstep.collectionsphotos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public UserFullDto findById(@PathVariable int id){
        UserFullDto userFullDto = userService.findById(id);
        return userFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAllUsers(){
        List<UserShortDto> allUsers = userService.findAll();
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value = "/users/search", method = RequestMethod.GET)
    public UserFullDto findByEmail(@RequestParam String email){
        UserFullDto user = userService.findByEmail(email);
        return user;
    }


    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserFullDto create(@Valid @RequestBody UserCreateDto createRequest){
        UserFullDto createUser = userService.create(createRequest);
        return createUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserFullDto update(@Valid @RequestBody UserUpdateDto updateRequest){
        UserFullDto updateUser = userService.update(updateRequest);
        return updateUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable int id){
        userService.deleteById(id);
    }


}
