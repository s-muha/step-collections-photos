package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.collectionDto.*;
import by.itstep.collectionsphotos.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    CollectionService collectionService;

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.GET)
    public CollectionFullDto findById(@PathVariable int id){
        CollectionFullDto collectionFullDto = collectionService.findById(id);
        return collectionFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.GET)
    public List<CollectionShortDto> findAllCollections(){
        List<CollectionShortDto> collectionShortDtos = collectionService.findAll();
        return collectionShortDtos;
    }

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.POST)
    public CollectionFullDto create(@Valid @RequestBody CollectionCreateDto collectionCreateDto){
        CollectionFullDto collectionFullDto = collectionService.create(collectionCreateDto);
        return collectionFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/collections",method = RequestMethod.PUT)
    public CollectionFullDto update(@Valid @RequestBody CollectionUpdateDto collectionUpdateDto){
        CollectionFullDto collectionFullDto = collectionService.update(collectionUpdateDto);
        return collectionFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        collectionService.deleteById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/collections/addPhotoToCollection", method = RequestMethod.PUT)
    public CollectionFullDto addPhotoToCollection(@RequestBody CollectionCreatePhotoInCollectionDto collectionCreatePhotoDto){
        CollectionFullDto collectionFullDto = collectionService.addPhotoToCollection(collectionCreatePhotoDto);
        return collectionFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "collections/removePhotoFromCollection", method = RequestMethod.PUT)
    public CollectionFullDto removePhotoFromCollection(@RequestBody CollectionRemovePhotoInCollectionDto
                                                                   collectionRemovePhotoInCollectionDto){
        CollectionFullDto collectionFullDto = collectionService.removePhotoFromCollection(collectionRemovePhotoInCollectionDto);
        return collectionFullDto;
    }

}
