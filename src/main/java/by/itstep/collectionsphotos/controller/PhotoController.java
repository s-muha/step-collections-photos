package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.photoDto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoUpdateDto;
import by.itstep.collectionsphotos.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PhotoController {

    @Autowired
    PhotoService photoService;

    @ResponseBody
    @RequestMapping(value = "/photos/{id}", method = RequestMethod.GET)
    public PhotoFullDto findById(@PathVariable int id){
        PhotoFullDto photoFullDto = photoService.findById(id);
        return photoFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.GET)
    public List<PhotoShortDto> findAllPhotos(){
        List<PhotoShortDto> photoShortDtos = photoService.findAll();
        return photoShortDtos;
    }

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.POST)
    public PhotoFullDto create(@Valid @RequestBody PhotoCreateDto photoCreateDto){
        PhotoFullDto photoFullDto = photoService.create(photoCreateDto);
        return photoFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.PUT)
    public PhotoFullDto update(@Valid @RequestBody PhotoUpdateDto photoUpdateDto){
        PhotoFullDto photoFullDto = photoService.update(photoUpdateDto);
        return photoFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/photos/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        photoService.deleteById(id);
    }

}
