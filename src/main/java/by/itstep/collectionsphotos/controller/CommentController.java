package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.commentDto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentUpdateDto;
import by.itstep.collectionsphotos.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;


    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
    public CommentFullDto findById(@PathVariable int id){
        CommentFullDto commentFullDto = commentService.findById(id);
        return commentFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentShortDto> findAllComments(){
        List<CommentShortDto> allCommentDtos = commentService.findAll();
        return allCommentDtos;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.POST)
    public CommentFullDto create(@Valid @RequestBody CommentCreateDto commentCreateDto){
        CommentFullDto commentFullDto = commentService.create(commentCreateDto);
        return commentFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.PUT)
    public CommentFullDto update(@Valid @RequestBody CommentUpdateDto commentUpdateDto){
        CommentFullDto updateComment = commentService.update(commentUpdateDto);
        return updateComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable int id){
        commentService.deleteById(id);
    }

}
