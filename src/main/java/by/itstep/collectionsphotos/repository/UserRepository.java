package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public interface UserRepository {

    UserEntity findById(int id);

    public UserEntity findByEmail(String email);

    List<UserEntity> findAll();

    UserEntity create(UserEntity entity);

    UserEntity update(UserEntity entity);

    void deleteById(int id);

    void deleteAll();


}
