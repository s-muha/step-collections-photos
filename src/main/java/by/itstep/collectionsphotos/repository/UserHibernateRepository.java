package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository {

    @Override
    public UserEntity findById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        if(foundUser != null) {
            Hibernate.initialize(foundUser.getCollections());
            Hibernate.initialize(foundUser.getComments());
        }
        em.getTransaction().commit();
        em.close();
        return foundUser;
    }

    @Override
    public UserEntity findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

//        Object objectUser = em.createNativeQuery("SELECT * FROM users WHERE users.email = '" + email + "'",
//                UserEntity.class).getSingleResult();
//
//        UserEntity user = (UserEntity) objectUser;

        List<UserEntity> foundUsers = em.createNativeQuery("SELECT * FROM users WHERE users.email = '" + email + "'",
                UserEntity.class).getResultList();

        if(foundUsers.size() == 1){
            UserEntity user = foundUsers.get(0);
            Hibernate.initialize(user.getCollections());
            Hibernate.initialize(user.getComments());
            return user;
        }
        em.getTransaction().commit();
        em.close();
        return  null;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<UserEntity> allUsers = em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();

        if(allUsers != null) {
            for (UserEntity user : allUsers) {
                Hibernate.initialize(user.getCollections());
                Hibernate.initialize(user.getComments());
            }
        }
        em.getTransaction().commit();
        em.close();

        return allUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);// -> ex

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

      //  UserEntity user = em.find(UserEntity.class, entity.getId());

//        user.setLogin(entity.getLogin());
//        user.setPassword(entity.getPassword());
//        user.setEmail(entity.getEmail());
//        user.setName(entity.getName());

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.remove(em.find(UserEntity.class, id));

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
