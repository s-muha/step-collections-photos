package by.itstep.collectionsphotos;

import by.itstep.collectionsphotos.service.UserService;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollectionsPhotosApplication {

    public static void main(String[] args) {
        SpringApplication.run(CollectionsPhotosApplication.class, args);

    }

}
