package by.itstep.collectionsphotos.dto.photoDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PhotoUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    @NotNull
    private String name;
}
