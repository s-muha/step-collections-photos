package by.itstep.collectionsphotos.dto.photoDto;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class PhotoCreateDto {

    @URL(message = "URL must have valid format")
    @NotNull
    private String link;

    @NotBlank
    private String name;

}
