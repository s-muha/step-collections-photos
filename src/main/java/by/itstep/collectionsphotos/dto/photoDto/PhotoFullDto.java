package by.itstep.collectionsphotos.dto.photoDto;

import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import lombok.Data;

import java.util.List;

@Data
public class PhotoFullDto {

    private Integer id;
    private String link;
    private String name;
    private Integer rating;
    private List<CommentShortDto> comments;
}
