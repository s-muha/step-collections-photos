package by.itstep.collectionsphotos.dto.photoDto;

import lombok.Data;

@Data
public class PhotoShortDto {
    private Integer id;
    private String link; //TODO добавить
    private String name;
    private Integer rating;

}
