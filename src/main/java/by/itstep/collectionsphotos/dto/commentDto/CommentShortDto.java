package by.itstep.collectionsphotos.dto.commentDto;

import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import lombok.Data;

@Data
public class CommentShortDto {

    private Integer id;
    private String message;
    private UserShortDto user;
}
