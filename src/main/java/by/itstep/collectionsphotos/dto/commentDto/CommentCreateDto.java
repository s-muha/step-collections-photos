package by.itstep.collectionsphotos.dto.commentDto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CommentCreateDto {

    @NotNull
    @Size(max = 200, message = "Massage length must be no more than 200")
    private String message;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer photoId;
}
