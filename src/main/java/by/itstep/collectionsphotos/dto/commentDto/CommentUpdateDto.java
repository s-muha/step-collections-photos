package by.itstep.collectionsphotos.dto.commentDto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CommentUpdateDto {

    @NotNull
    private Integer id;

    @NotNull
    private String message;
}
