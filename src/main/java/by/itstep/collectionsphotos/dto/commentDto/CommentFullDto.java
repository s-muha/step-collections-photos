package by.itstep.collectionsphotos.dto.commentDto;

import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import lombok.Data;

@Data
public class CommentFullDto {

    private Integer id;
    private String message;
    private UserShortDto user;
    private PhotoShortDto photo;
}
