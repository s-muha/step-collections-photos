package by.itstep.collectionsphotos.dto.userDto;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class UserCreateDto {

    @NotBlank(message = "login can not be blank")
    @Size(min = 3, max = 20, message = "Login length must be between 3 and 20")
    private String login;

    @Email(message = "Email must have valid format")
    @NotNull(message = "Email can not be null")
    private String email;

    @NotBlank
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;

    @NotBlank
    private String name;

}
