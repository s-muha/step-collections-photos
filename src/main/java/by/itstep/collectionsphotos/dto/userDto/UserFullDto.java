package by.itstep.collectionsphotos.dto.userDto;

import by.itstep.collectionsphotos.dto.collectionDto.CollectionShortDto;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Integer id;
    private String name;
    private String login;
    private String email;
    private List<CollectionShortDto> collections;
}
