package by.itstep.collectionsphotos.dto.userDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank(message = "login can not be blank")
    @Size(min = 3, max = 20, message = "Login length must be between 3 and 20")
    private String login;
}
