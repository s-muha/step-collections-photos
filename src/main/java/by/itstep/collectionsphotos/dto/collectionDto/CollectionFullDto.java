package by.itstep.collectionsphotos.dto.collectionDto;

import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import lombok.Data;

import java.util.List;

@Data
public class CollectionFullDto {

    private Integer id;
    private String name;
    private String description;
    private UserShortDto user;
    private List<PhotoShortDto> photos;
}
