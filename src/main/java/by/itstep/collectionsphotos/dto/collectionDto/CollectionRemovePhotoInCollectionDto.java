package by.itstep.collectionsphotos.dto.collectionDto;

import lombok.Data;

@Data
public class CollectionRemovePhotoInCollectionDto {

    private Integer collectionId;
    private Integer photoId;
}
