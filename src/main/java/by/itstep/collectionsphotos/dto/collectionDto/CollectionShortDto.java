package by.itstep.collectionsphotos.dto.collectionDto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CollectionShortDto {

    private Integer id;
    private String name;
    private String description;
}
