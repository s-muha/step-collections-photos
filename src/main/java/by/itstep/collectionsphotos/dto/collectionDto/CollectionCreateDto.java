package by.itstep.collectionsphotos.dto.collectionDto;

import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CollectionCreateDto {

    @NotBlank(message = "name can not be blank")
    @Size(min = 3, max = 20, message = "Name length must be between 3 and 20")
    private String name;

    @Size(max = 200, message = "Description length must be no more than 200")
    private String description;

    @NotNull(message = "UserId can not be null")
    private Integer userId;

}
