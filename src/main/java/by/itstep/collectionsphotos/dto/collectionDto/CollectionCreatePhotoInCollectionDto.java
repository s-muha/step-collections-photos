package by.itstep.collectionsphotos.dto.collectionDto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CollectionCreatePhotoInCollectionDto {

    @NotNull
    private Integer collectionId;

    @NotNull
    private Integer photoId;
}
