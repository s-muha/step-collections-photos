package by.itstep.collectionsphotos.utils;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.repository.*;


import java.util.ArrayList;


public class DatabaseCleaner {


    private UserRepository userRepository = new UserHibernateRepository();


    private PhotoRepository photoRepository = new PhotoHibernateRepository();


    private CommentRepository commentRepository = new CommentHibernateRepository();


    private CollectionRepository collectionRepository = new CollectionHibernateRepository();


    public void clean(){

        for(CollectionEntity collection : collectionRepository.findAll()){
            collection.setPhotos(new ArrayList<>());
            collectionRepository.update(collection);
        }

        collectionRepository.deleteAll();
        commentRepository.deleteAll();
        photoRepository.deleteAll();
        userRepository.deleteAll();
    }

}
