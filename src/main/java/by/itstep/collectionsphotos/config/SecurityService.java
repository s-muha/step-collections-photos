package by.itstep.collectionsphotos.config;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    @Autowired
    UserRepository userRepository;

    public UserEntity getAuthenticateUser(){
        String email = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
        return userRepository.findByEmail(email);
    }




}
