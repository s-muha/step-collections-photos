package by.itstep.collectionsphotos.repository;


import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;


    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        CommentEntity existingComment = addCommentToDb();
        Integer id = existingComment.getId();
        //when
        CommentEntity foundComment = commentRepository.findById(id);
        //then
        Assertions.assertEquals(id, foundComment.getId());

        Assertions.assertEquals(existingComment.getMessage(), foundComment.getMessage());
        Assertions.assertEquals(existingComment.getUser(), foundComment.getUser());
        Assertions.assertEquals(existingComment.getPhoto(), foundComment.getPhoto());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<CommentEntity> foundComments = commentRepository.findAll();
        //then

        Assertions.assertTrue(foundComments.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        CommentEntity comment1 = addCommentToDb();
        CommentEntity comment2 = addCommentToDb();
        CommentEntity comment3 = addCommentToDb();
        CommentEntity comment4 = addCommentToDb();
        CommentEntity comment5 = addCommentToDb();

        commentRepository.deleteById(comment1.getId());
        commentRepository.deleteById(comment2.getId());
        // when
        List<CommentEntity> commentsFoundInTheDb = commentRepository.findAll();
        //then
        Assertions.assertEquals(3, commentsFoundInTheDb.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity userToSave = EntityUtils.prepareUser();
        userRepository.create(userToSave);

        PhotoEntity photoToSave = EntityUtils.preparePhoto();
        photoRepository.create(photoToSave);

        CommentEntity commentToSave = EntityUtils.prepareComment(userToSave, photoToSave);

        // when
        CommentEntity saveComment = commentRepository.create(commentToSave);
        //then

        Assertions.assertNotNull(saveComment.getId());
        Assertions.assertNotNull(saveComment.getMessage());
        CommentEntity commentFoundInTheDb = commentRepository.findById(saveComment.getId());

        Assertions.assertEquals(saveComment.getMessage(), commentFoundInTheDb.getMessage(), commentToSave.getMessage());
        Assertions.assertEquals(saveComment.getUser(), commentFoundInTheDb.getUser());
        Assertions.assertEquals(saveComment.getPhoto(), commentFoundInTheDb.getPhoto());

        Assertions.assertEquals(commentToSave.getUser(), commentFoundInTheDb.getUser());
        Assertions.assertEquals(commentToSave.getPhoto(), commentFoundInTheDb.getPhoto());

        Assertions.assertEquals(userToSave.getId(), commentFoundInTheDb.getUser().getId());
        Assertions.assertEquals(photoToSave.getId(), commentFoundInTheDb.getPhoto().getId());

    }

    @Test
    public void update_happyPatch() {
        // given
        CommentEntity existingComment = addCommentToDb();
        existingComment.setMessage("updated Message");

        // when
        CommentEntity updatedComment = commentRepository.update(existingComment);
        //then
        Assertions.assertEquals(existingComment.getId(), updatedComment.getId());
        CommentEntity foundComment = commentRepository.findById(existingComment.getId());

        Assertions.assertEquals(existingComment.getMessage(), foundComment.getMessage());
        Assertions.assertEquals(existingComment.getUser(), foundComment.getUser());
        Assertions.assertEquals(existingComment.getPhoto(), foundComment.getPhoto());

    }

    @Test
    public void delete_happyPatch() {
        // given
        CommentEntity existingComment = addCommentToDb();
        // when
        commentRepository.deleteById(existingComment.getId());
        //then

        CommentEntity foundComment = commentRepository.findById(existingComment.getId());
        Assertions.assertNull(foundComment);
    }

    private CommentEntity addCommentToDb() {
        UserEntity user = EntityUtils.prepareUser();
        PhotoEntity photo = EntityUtils.preparePhoto();
        userRepository.create(user);
        photoRepository.create(photo);
        CommentEntity commentToAdd = EntityUtils.prepareComment(user, photo);

        return commentRepository.create(commentToAdd);
    }

}
