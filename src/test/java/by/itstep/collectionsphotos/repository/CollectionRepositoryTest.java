package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CollectionRepositoryTest {

    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        CollectionEntity existingCollection = addCollectionToDb();
        Integer id = existingCollection.getId();
        //when
        CollectionEntity foundCollection = collectionRepository.findById(id);
        //then
        Assertions.assertEquals(id, foundCollection.getId());

        Assertions.assertEquals(existingCollection.getName(), foundCollection.getName());
        Assertions.assertEquals(existingCollection.getDescription(), foundCollection.getDescription());
        Assertions.assertEquals(existingCollection.getUser(), foundCollection.getUser());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<CollectionEntity> foundCollections = collectionRepository.findAll();
        //then
        Assertions.assertTrue(foundCollections.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        CollectionEntity collection1 = addCollectionToDb();
        CollectionEntity collection2 = addCollectionToDb();
        CollectionEntity collection3 = addCollectionToDb();
        CollectionEntity collection4 = addCollectionToDb();
        CollectionEntity collection5 = addCollectionToDb();

        collectionRepository.deleteById(collection1.getId());
        collectionRepository.deleteById(collection2.getId());

        // when
        List<CollectionEntity> foundCollections = collectionRepository.findAll();
        //then
        Assertions.assertEquals(3, foundCollections.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity userToSave = EntityUtils.prepareUser();
        userRepository.create(userToSave);

        CollectionEntity collectionToSave = EntityUtils.prepareCollection(userToSave);
        // when
        CollectionEntity saveCollection = collectionRepository.create(collectionToSave);
        //then
        Assertions.assertNotNull(saveCollection.getId());

        CollectionEntity collectionFoundInTheDb = collectionRepository.findById(saveCollection.getId());

        Assertions.assertEquals(saveCollection.getName(), collectionFoundInTheDb.getName(), collectionToSave.getName());
        Assertions.assertEquals(saveCollection.getDescription(), collectionFoundInTheDb.getDescription(),
                collectionToSave.getDescription());
        Assertions.assertEquals(saveCollection.getUser(), collectionFoundInTheDb.getUser());
        Assertions.assertEquals(collectionFoundInTheDb.getUser(), collectionToSave.getUser());

        Assertions.assertEquals(userToSave.getId(), collectionFoundInTheDb.getUser().getId());
    }

    @Test
    public void update_happyPatch() {
        // given
        CollectionEntity existingCollection = addCollectionToDb();
        existingCollection.setDescription("updated Description");
        existingCollection.setName("updated Name");

        // when
        CollectionEntity updatedCollection = collectionRepository.update(existingCollection);
        //then
        Assertions.assertEquals(existingCollection.getId(), updatedCollection.getId());
        CollectionEntity foundCollection = collectionRepository.findById(existingCollection.getId());

        Assertions.assertEquals(existingCollection.getName(), foundCollection.getName());
        Assertions.assertEquals(existingCollection.getDescription(), foundCollection.getDescription());
        Assertions.assertEquals(existingCollection.getUser(), foundCollection.getUser());

    }

    @Test
    public void delete_happyPatch() {
        // given
        CollectionEntity existingCollection = addCollectionToDb();
        // when
        collectionRepository.deleteById(existingCollection.getId());
        //then

        CollectionEntity foundCollection = collectionRepository.findById(existingCollection.getId());
        Assertions.assertNull(foundCollection);
    }

    private CollectionEntity addCollectionToDb() {
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        CollectionEntity collectionToAdd = EntityUtils.prepareCollection(user);
        return collectionRepository.create(collectionToAdd);
    }

}
