package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.PersistenceException;
import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

//    @AfterEach
//    public void shutDown() {
//        dbCleaner.clean();
//    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        Integer id = existingUser.getId();
        //when

        UserEntity foundUser = userRepository.findById(id);
        //then

        Assertions.assertEquals(id, foundUser.getId());

        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getName(), foundUser.getName());

    }

    @Test
    public void findByEmail_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        String email = existingUser.getEmail();
        //when

        UserEntity foundUser = userRepository.findByEmail(email);
        //then

        Assertions.assertEquals(email, foundUser.getEmail());

        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getName(), foundUser.getName());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<UserEntity> foundUsers = userRepository.findAll();
        //then
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPatch() {
        // given
        UserEntity user1 = addUserToDb();
        UserEntity user2 = addUserToDb();
        UserEntity user3 = addUserToDb();
        UserEntity user4 = addUserToDb();
        UserEntity user5 = addUserToDb();
        userRepository.deleteById(user1.getId());
        userRepository.deleteById(user2.getId());
        // when
        List<UserEntity> usersFoundInTheDb = userRepository.findAll();
        //then
        Assertions.assertEquals(3, usersFoundInTheDb.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity userToSave = EntityUtils.prepareUser();
        // when
        UserEntity saveUser = userRepository.create(userToSave);
        //then
        Assertions.assertNotNull(saveUser.getId());

        UserEntity userFoundInTheDb = userRepository.findById(saveUser.getId());

        Assertions.assertEquals(userToSave.getName(), saveUser.getName(), userFoundInTheDb.getName());
        Assertions.assertEquals(userToSave.getPassword(), saveUser.getPassword(), userFoundInTheDb.getPassword());
        Assertions.assertEquals(userToSave.getEmail(), saveUser.getEmail(), userFoundInTheDb.getEmail());
        Assertions.assertEquals(userToSave.getLogin(), saveUser.getLogin(), userFoundInTheDb.getLogin());
    }

    @Test
    public void update_happyPatch() {
        // given
        UserEntity existingUser = addUserToDb();

        existingUser.setEmail("updated Email");
        existingUser.setLogin("updated Login");
        existingUser.setPassword("updated Password");

        // when
        UserEntity updatedUser = userRepository.update(existingUser);
        //then
        Assertions.assertEquals(existingUser.getId(), updatedUser.getId());
        UserEntity foundUser = userRepository.findById(existingUser.getId());

        Assertions.assertEquals(existingUser.getName(), foundUser.getName());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
    }

    @Test
    public void delete_happyPatch() {
        // given
        UserEntity existingUser = addUserToDb();
        // when

        userRepository.deleteById(existingUser.getId());

        //then

        UserEntity foundUser = userRepository.findById(existingUser.getId());
        Assertions.assertNull(foundUser);
    }

    private UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }

}
