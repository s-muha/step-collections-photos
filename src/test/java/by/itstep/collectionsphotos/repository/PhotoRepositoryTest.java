package by.itstep.collectionsphotos.repository;


import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PhotoRepositoryTest {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    private void shutDown(){
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        // given
        PhotoEntity existingPhoto = addPhotoToDb();
        Integer id = existingPhoto.getId();
        // when
        PhotoEntity foundPhoto = photoRepository.findById(id);
        // then
        Assertions.assertEquals(id, foundPhoto.getId());

        Assertions.assertEquals(existingPhoto.getLink(), foundPhoto.getLink());
        Assertions.assertEquals(existingPhoto.getName(), foundPhoto.getName());
        Assertions.assertEquals(existingPhoto.getRating(), foundPhoto.getRating());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<PhotoEntity> foundPhotos = photoRepository.findAll();
        //then
        Assertions.assertTrue(foundPhotos.isEmpty());
    }

    @Test
    public void findAll_happyPatch() {
        // given
        PhotoEntity photo1 = addPhotoToDb();
        PhotoEntity photo2 = addPhotoToDb();
        PhotoEntity photo3 = addPhotoToDb();
        PhotoEntity photo4 = addPhotoToDb();
        PhotoEntity photo5 = addPhotoToDb();

        photoRepository.deleteById(photo1.getId());
        photoRepository.deleteById(photo2.getId());
        // when
        List<PhotoEntity> photosFoundInTheDb = photoRepository.findAll();
        //then
        Assertions.assertEquals(3, photosFoundInTheDb.size());
    }

    @Test
    public void create_happyPath() {
        // given
        PhotoEntity photoToSave = EntityUtils.preparePhoto();
        // when
        PhotoEntity savePhoto = photoRepository.create(photoToSave);
        //then
        Assertions.assertNotNull(savePhoto.getId());

        PhotoEntity photoFoundInTheDb = photoRepository.findById(savePhoto.getId());

        Assertions.assertEquals(photoToSave.getLink(), savePhoto.getLink(), photoFoundInTheDb.getLink());
        Assertions.assertEquals(photoToSave.getName(), savePhoto.getName(), photoFoundInTheDb.getName());
        Assertions.assertEquals(photoToSave.getRating(), savePhoto.getRating(), photoFoundInTheDb.getRating());

    }

    @Test
    public void update_happyPatch() {
        // given
        PhotoEntity existingPhoto = addPhotoToDb();
        existingPhoto.setLink("updated Link");
        existingPhoto.setName("updated Name");
        existingPhoto.setRating(10);
        // when
        PhotoEntity updatedPhoto = photoRepository.update(existingPhoto);
        //then
        Assertions.assertEquals(existingPhoto.getId(), updatedPhoto.getId());
        PhotoEntity foundPhoto = photoRepository.findById(existingPhoto.getId());

        Assertions.assertEquals(updatedPhoto.getLink(), foundPhoto.getLink());
        Assertions.assertEquals(updatedPhoto.getName(), foundPhoto.getName());
        Assertions.assertEquals(updatedPhoto.getRating(), foundPhoto.getRating());

    }

    @Test
    public void delete_happyPatch() {
        // given
        PhotoEntity existingPhoto = addPhotoToDb();
        // when
        photoRepository.deleteById(existingPhoto.getId());

        //then
        PhotoEntity foundPhoto = photoRepository.findById(existingPhoto.getId());
        Assertions.assertNull(foundPhoto);
    }

    private PhotoEntity addPhotoToDb() {
        PhotoEntity photoToAdd = EntityUtils.preparePhoto();
        return photoRepository.create(photoToAdd);
    }
}
