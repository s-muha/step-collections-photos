package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.collectionDto.CollectionFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentShortDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentServiceTest {

    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        CommentEntity existingComment = addCommentToDb();
        Integer id = existingComment.getId();
        //when
        CommentFullDto foundComment = commentService.findById(id);
        //then
        Assertions.assertEquals(id, foundComment.getId());

        Assertions.assertEquals(existingComment.getMessage(), foundComment.getMessage());
        Assertions.assertEquals(existingComment.getUser().getId(), foundComment.getUser().getId());
        Assertions.assertEquals(existingComment.getPhoto().getId(), foundComment.getPhoto().getId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<CommentShortDto> foundComments = commentService.findAll();
        //then

        Assertions.assertTrue(foundComments.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        UserEntity user1 = EntityUtils.prepareUser();
        UserEntity user2 = EntityUtils.prepareUser();
        UserEntity user3 = EntityUtils.prepareUser();

        PhotoEntity photo1 = EntityUtils.preparePhoto();
        PhotoEntity photo2 = EntityUtils.preparePhoto();
        PhotoEntity photo3 = EntityUtils.preparePhoto();

        userRepository.create(user1);
        userRepository.create(user2);
        userRepository.create(user3);

        photoRepository.create(photo1);
        photoRepository.create(photo2);
        photoRepository.create(photo3);

        CommentEntity comment1 = EntityUtils.prepareComment(user1, photo1);
        CommentEntity comment2 = EntityUtils.prepareComment(user2, photo2);
        CommentEntity comment3 = EntityUtils.prepareComment(user3, photo3);

        CommentCreateDto commentCreateDto1 = commentMapper.mapCommentCreateDto(comment1);
        CommentCreateDto commentCreateDto2 = commentMapper.mapCommentCreateDto(comment2);
        CommentCreateDto commentCreateDto3 = commentMapper.mapCommentCreateDto(comment3);

        commentService.create(commentCreateDto1);
        commentService.create(commentCreateDto2);
        commentService.create(commentCreateDto3);

        // when
        List<CommentShortDto> foundComments = commentService.findAll();
        //then
        Assertions.assertEquals(3, foundComments.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = EntityUtils.preparePhoto();
        photoRepository.create(photo);

        CommentEntity commentToSave = EntityUtils.prepareComment(user, photo);
        CommentCreateDto commentCreateDtoToSave = new CommentCreateDto();
        commentCreateDtoToSave.setUserId(user.getId());
        commentCreateDtoToSave.setPhotoId(photo.getId());
        commentCreateDtoToSave.setMessage(commentToSave.getMessage());

        // when
        CommentFullDto saveComment = commentService.create(commentCreateDtoToSave);
        //then

        Assertions.assertNotNull(saveComment.getId());
        Assertions.assertNotNull(saveComment.getMessage());
        Assertions.assertNotNull(saveComment.getPhoto());
        Assertions.assertNotNull(saveComment.getUser());
        CommentFullDto foundComment = commentService.findById(saveComment.getId());

        Assertions.assertEquals(foundComment.getMessage(), saveComment.getMessage(), commentToSave.getMessage());
        Assertions.assertEquals(foundComment.getUser(), saveComment.getUser());
        Assertions.assertEquals(foundComment.getPhoto(), saveComment.getPhoto());

        Assertions.assertEquals(user.getId(), foundComment.getUser().getId());
        Assertions.assertEquals(photo.getId(), foundComment.getPhoto().getId());

    }

    @Test
    public void update_happyPatch() {
        // given
        CommentEntity existingComment = addCommentToDb();
        CommentUpdateDto commentUpdateDto = commentMapper.mapCommentUpdateDto(existingComment);
        commentUpdateDto.setMessage("updated Message");
        // when
        CommentFullDto updatedComment = commentService.update(commentUpdateDto);
        //then
        Assertions.assertEquals(updatedComment.getId(), existingComment.getId());
        CommentFullDto foundComment = commentService.findById(existingComment.getId());

        Assertions.assertEquals(commentUpdateDto.getMessage(), foundComment.getMessage(), updatedComment.getMessage());
        Assertions.assertEquals(existingComment.getUser().getId(), foundComment.getUser().getId());
        Assertions.assertEquals(existingComment.getPhoto().getId(), foundComment.getPhoto().getId());
    }

    @Test
    public void delete_happyPatch() {
        // given
        UserEntity user = EntityUtils.prepareUser();
        PhotoEntity photo = EntityUtils.preparePhoto();

        userRepository.create(user);
        photoRepository.create(photo);

        CommentEntity existingComment = EntityUtils.prepareComment(user, photo);

        commentRepository.create(existingComment);

        // when
        commentService.deleteById(existingComment.getId());
        //then
        CommentEntity foundComment = commentRepository.findById(existingComment.getId());
        Assertions.assertNull(foundComment);
        Assertions.assertNotNull(user);
        Assertions.assertNotNull(photo);
    }

    private CommentEntity addCommentToDb() {
        UserEntity user = EntityUtils.prepareUser();
        PhotoEntity photo = EntityUtils.preparePhoto();
        userRepository.create(user);
        photoRepository.create(photo);
        CommentEntity commentToAdd = EntityUtils.prepareComment(user, photo);

        return commentRepository.create(commentToAdd);
    }

}
