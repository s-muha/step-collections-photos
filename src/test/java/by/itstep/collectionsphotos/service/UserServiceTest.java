package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.userDto.UserCreateDto;
import by.itstep.collectionsphotos.dto.userDto.UserFullDto;
import by.itstep.collectionsphotos.dto.userDto.UserShortDto;
import by.itstep.collectionsphotos.dto.userDto.UserUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.mapper.UserMapper;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private UserMapper userMapper;

//    @BeforeEach
//    private void setUp() {
//        dbCleaner.clean();
//    }

//    @AfterEach
//    public void shutDown() {
//        dbCleaner.clean();
//    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        Integer id = existingUser.getId();
        //when

        UserFullDto foundUser = userService.findById(id);
        //then

        Assertions.assertEquals(id, foundUser.getId());

        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getName(), foundUser.getName());

    }

    @Test
    public void findByEmail_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        String email = existingUser.getEmail();
        //when

        UserFullDto foundUser = userService.findByEmail(email);
        //then

        Assertions.assertEquals(email, foundUser.getEmail());

        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getName(), foundUser.getName());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<UserShortDto> foundUsers = userService.findAll();
        //then

        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPatch() {
        // given
        addUserToDb();
        addUserToDb();
        // when
        List<UserShortDto> foundUsers = userService.findAll();
        //then
        Assertions.assertEquals(2, foundUsers.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity userEntityToSave = EntityUtils.prepareUser();

        UserCreateDto userCreateDtoToSave = userMapper.mapUserCreateDto(userEntityToSave);
        userCreateDtoToSave.setName(userEntityToSave.getName());
        userCreateDtoToSave.setLogin(userEntityToSave.getLogin());
        userCreateDtoToSave.setEmail(userEntityToSave.getEmail());
        userCreateDtoToSave.setPassword(userEntityToSave.getPassword());
        // when
        UserFullDto saveUser = userService.create(userCreateDtoToSave);
        //then
        Assertions.assertNotNull(saveUser.getId());

        UserFullDto foundUser = userService.findById(saveUser.getId());

        Assertions.assertEquals(foundUser.getName(), saveUser.getName(), userCreateDtoToSave.getName());
        Assertions.assertEquals(foundUser.getEmail(), saveUser.getEmail(), userCreateDtoToSave.getEmail());
        Assertions.assertEquals(foundUser.getLogin(), saveUser.getLogin(), userCreateDtoToSave.getLogin());
    }

    @Test
    public void update_happyPatch() {
        // given

        UserEntity existingUser = addUserToDb();
        UserUpdateDto userUpdateDto = userMapper.mapUserUpdateDto(existingUser);

        userUpdateDto.setName("updated Name");
        userUpdateDto.setLogin("updated Login");

        // when
        UserFullDto updatedUser = userService.update(userUpdateDto);
        //then
        Assertions.assertEquals(userUpdateDto.getId(), updatedUser.getId());
        UserFullDto foundUser = userService.findById(existingUser.getId());

        Assertions.assertEquals(userUpdateDto.getName(), foundUser.getName());
        Assertions.assertEquals(userUpdateDto.getLogin(), foundUser.getLogin());

    }

    @Test
    public void delete_happyPatch() {
        // given
        UserEntity deletedUser = addUserToDb();

        PhotoEntity photo1DeletedUser = photoRepository.create(EntityUtils.preparePhoto());
        PhotoEntity photo2DeletedUser = photoRepository.create(EntityUtils.preparePhoto());
        PhotoEntity photo3DeletedUser = photoRepository.create(EntityUtils.preparePhoto());
        PhotoEntity photo4DeletedUser = photoRepository.create(EntityUtils.preparePhoto());

        List<PhotoEntity> photos1DeletedUser = new ArrayList<>();
        photos1DeletedUser.add(photo1DeletedUser);
        photos1DeletedUser.add(photo2DeletedUser);

        List<PhotoEntity> photos2DeletedUser = new ArrayList<>();
        photos2DeletedUser.add(photo3DeletedUser);
        photos2DeletedUser.add(photo4DeletedUser);

        CollectionEntity collection1DeletedUser = EntityUtils.prepareCollection(deletedUser);
        CollectionEntity collection2DeletedUser = EntityUtils.prepareCollection(deletedUser);

        collection1DeletedUser.setPhotos(photos1DeletedUser);
        collection2DeletedUser.setPhotos(photos2DeletedUser);

        collectionRepository.create(collection1DeletedUser);
        collectionRepository.create(collection2DeletedUser);

        CommentEntity commentDeletedUser1 = EntityUtils.prepareComment(deletedUser, photo1DeletedUser);
        CommentEntity commentDeletedUser2 = EntityUtils.prepareComment(deletedUser, photo2DeletedUser);
        CommentEntity commentDeletedUser3 = EntityUtils.prepareComment(deletedUser, photo3DeletedUser);
        CommentEntity commentDeletedUser4 = EntityUtils.prepareComment(deletedUser, photo4DeletedUser);

        commentRepository.create(commentDeletedUser1);
        commentRepository.create(commentDeletedUser2);
        commentRepository.create(commentDeletedUser3);
        commentRepository.create(commentDeletedUser4);
        // when
        //userService.deleteById(deletedUser.getId());
        //then
        UserEntity foundUserToDb = userRepository.findById(deletedUser.getId());
        Assertions.assertNull(foundUserToDb);

        Assertions.assertNull(photoRepository.findById(photo1DeletedUser.getId()));
        Assertions.assertNull(photoRepository.findById(photo2DeletedUser.getId()));
        Assertions.assertNull(photoRepository.findById(photo3DeletedUser.getId()));
        Assertions.assertNull(photoRepository.findById(photo4DeletedUser.getId()));

        Assertions.assertNull(collectionRepository.findById(collection1DeletedUser.getId()));
        Assertions.assertNull(collectionRepository.findById(collection2DeletedUser.getId()));

        Assertions.assertNull(collectionRepository.findById(commentDeletedUser1.getId()));
        Assertions.assertNull(collectionRepository.findById(commentDeletedUser2.getId()));
        Assertions.assertNull(collectionRepository.findById(commentDeletedUser3.getId()));
        Assertions.assertNull(collectionRepository.findById(commentDeletedUser4.getId()));
    }

    private UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }
}
