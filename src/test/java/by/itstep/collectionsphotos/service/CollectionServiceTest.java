package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.collectionDto.CollectionCreateDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionFullDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionShortDto;
import by.itstep.collectionsphotos.dto.collectionDto.CollectionUpdateDto;
import by.itstep.collectionsphotos.dto.commentDto.CommentFullDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CollectionServiceTest {

    @Autowired
    private CollectionService collectionService;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private CollectionMapper collectionMapper;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

//    @AfterEach
//    public void shutDown() {
//        dbCleaner.clean();
//    }

    @Test
    public void findById_happyPath() {
        //given
        CollectionEntity existingCollection = addCollectionToDb();
        Integer id = existingCollection.getId();
        //when
        CollectionFullDto foundCollection = collectionService.findById(id);
        //then
        Assertions.assertEquals(id, foundCollection.getId());

        Assertions.assertEquals(existingCollection.getName(), foundCollection.getName());
        Assertions.assertEquals(existingCollection.getDescription(), foundCollection.getDescription());
        Assertions.assertEquals(existingCollection.getUser().getId(), foundCollection.getUser().getId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<CollectionShortDto> foundCollections = collectionService.findAll();
        //then
        Assertions.assertTrue(foundCollections.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        UserEntity user1 = EntityUtils.prepareUser();
        UserEntity user2 = EntityUtils.prepareUser();
        UserEntity user3 = EntityUtils.prepareUser();

        userRepository.create(user1);
        userRepository.create(user2);
        userRepository.create(user3);

        CollectionEntity collection1 = EntityUtils.prepareCollection(user1);
        CollectionEntity collection2 = EntityUtils.prepareCollection(user2);
        CollectionEntity collection3 = EntityUtils.prepareCollection(user3);

        CollectionCreateDto collectionCreateDto1 = collectionMapper.mapCollectionCreateDto(collection1);
        CollectionCreateDto collectionCreateDto2 = collectionMapper.mapCollectionCreateDto(collection2);
        CollectionCreateDto collectionCreateDto3 = collectionMapper.mapCollectionCreateDto(collection3);

        collectionService.create(collectionCreateDto1);
        collectionService.create(collectionCreateDto2);
        collectionService.create(collectionCreateDto3);

        // when
        List<CollectionShortDto> foundCollections = collectionService.findAll();
        //then
        Assertions.assertEquals(3, foundCollections.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);

        CollectionEntity collectionToSave = EntityUtils.prepareCollection(user);
        CollectionCreateDto collectionCreateDtoToSave = collectionMapper.mapCollectionCreateDto(collectionToSave);

        // when
        CollectionFullDto saveCollection = collectionService.create(collectionCreateDtoToSave);
        //then

        Assertions.assertNotNull(saveCollection.getId());

        CollectionEntity foundCollection = collectionRepository.findById(saveCollection.getId());

        Assertions.assertEquals(saveCollection.getName(), foundCollection.getName(), collectionToSave.getName());
        Assertions.assertEquals(saveCollection.getDescription(), foundCollection.getDescription(),
                collectionToSave.getDescription());
        Assertions.assertEquals(saveCollection.getUser().getId(), foundCollection.getUser().getId(),
                collectionToSave.getUser().getId());

    }

    @Test
    public void update_happyPatch() {
        // given
        CollectionEntity existingCollection = addCollectionToDb();
        CollectionUpdateDto collectionUpdateDto = collectionMapper.mapCollectionUpdateDto(existingCollection);
        existingCollection.setDescription("updated Description");
        existingCollection.setName("updated Name");

        // when
        CollectionFullDto updatedCollection = collectionService.update(collectionUpdateDto);
        //then
        Assertions.assertEquals(existingCollection.getId(), updatedCollection.getId());
        CollectionFullDto foundCollection = collectionService.findById(existingCollection.getId());

        Assertions.assertEquals(collectionUpdateDto.getName(), foundCollection.getName());
        Assertions.assertEquals(collectionUpdateDto.getDescription(), foundCollection.getDescription());
        Assertions.assertEquals(existingCollection.getUser().getId(), foundCollection.getUser().getId());

    }

    @Test
    public void delete_happyPatch() {
        // given
        CollectionEntity existingCollection = addCollectionToDb();
        UserEntity userEntity = existingCollection.getUser();
        // when
        collectionService.deleteById(existingCollection.getId());
        //then
        CollectionEntity foundCollection = collectionRepository.findById(existingCollection.getId());
        Assertions.assertNull(foundCollection);
        Assertions.assertNotNull(userEntity);
    }

    private CollectionEntity addCollectionToDb() {
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        CollectionEntity collectionToAdd = EntityUtils.prepareCollection(user);
        return collectionRepository.create(collectionToAdd);
    }
}
