package by.itstep.collectionsphotos.service;


import by.itstep.collectionsphotos.dto.photoDto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.photoDto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class PhotoServiceTest {

    @Autowired
    private PhotoService photoService;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private PhotoMapper photoMapper;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    private void shutDown(){
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        // given
        PhotoEntity existingPhoto = addPhotoToDb();
        Integer id = existingPhoto.getId();
        // when
        PhotoFullDto foundPhoto = photoService.findById(id);
        // then
        Assertions.assertEquals(id, foundPhoto.getId());

        Assertions.assertEquals(existingPhoto.getLink(), foundPhoto.getLink());
        Assertions.assertEquals(existingPhoto.getName(), foundPhoto.getName());
        Assertions.assertEquals(existingPhoto.getRating(), foundPhoto.getRating());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<PhotoShortDto> foundPhotos = photoService.findAll();
        //then
        Assertions.assertTrue(foundPhotos.isEmpty());
    }

    @Test
    public void findAll_happyPatch() {
        // given
        addPhotoToDb();
        addPhotoToDb();
        addPhotoToDb();
        // when
        List<PhotoShortDto> foundPhotos = photoService.findAll();
        //then
        Assertions.assertEquals(3, foundPhotos.size());
    }

    @Test
    public void create_happyPath() {
        // given
        PhotoEntity photoToSave = EntityUtils.preparePhoto();
        PhotoCreateDto photoCreateDtoToSave = photoMapper.mapPhotoCreateDto(photoToSave);
        // when
        PhotoFullDto savePhoto = photoService.create(photoCreateDtoToSave);
        //then
        Assertions.assertNotNull(savePhoto.getId());

        PhotoFullDto foundPhoto = photoService.findById(savePhoto.getId());

        Assertions.assertEquals(foundPhoto.getLink(), savePhoto.getLink(), photoToSave.getLink());
        Assertions.assertEquals(foundPhoto.getName(), savePhoto.getName(), photoToSave.getName());
        Assertions.assertEquals(foundPhoto.getRating(), savePhoto.getRating(), photoToSave.getRating());

    }

    @Test
    public void update_happyPatch() {
        // given
        PhotoEntity existingPhoto = addPhotoToDb();
        PhotoUpdateDto photoUpdateDto = photoMapper.mapPhotoUpdateDto(existingPhoto);
        photoUpdateDto.setName("updated Name");

        // when
        PhotoFullDto updatedPhoto = photoService.update(photoUpdateDto);
        //then
        Assertions.assertEquals(existingPhoto.getId(), updatedPhoto.getId(), photoUpdateDto.getId());
        PhotoFullDto foundPhoto = photoService.findById(existingPhoto.getId());

        Assertions.assertEquals(updatedPhoto.getLink(), foundPhoto.getLink());
        Assertions.assertEquals(updatedPhoto.getName(), foundPhoto.getName());
        Assertions.assertEquals(updatedPhoto.getRating(), foundPhoto.getRating());

    }

    @Test
    public void delete_happyPatch() {
        // given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);

        CollectionEntity collection = EntityUtils.prepareCollection(user);
        collectionRepository.create(collection);

        PhotoEntity photo = addPhotoToDb();
        collection.setPhotos(Arrays.asList(photo));
        collectionRepository.update(collection);

        // when
        photoService.deleteById(photo.getId());

        //then
        Assertions.assertNotNull(userRepository.findById(user.getId()));
        Assertions.assertNotNull(collectionRepository.findById(collection.getId()));

        Assertions.assertEquals(userRepository.findById(user.getId()), user);
        Assertions.assertEquals(collectionRepository.findById(collection.getId()), collection);

        PhotoEntity foundPhoto = photoRepository.findById(photo.getId());
        Assertions.assertNull(foundPhoto);
    }

    private PhotoEntity addPhotoToDb() {
        PhotoEntity photoToAdd = EntityUtils.preparePhoto();
        return photoRepository.create(photoToAdd);
    }

}
