package by.itstep.collectionsphotos.utils;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import com.github.javafaker.Faker;

public class EntityUtils {

    private static Faker faker = new Faker();

    public static UserEntity prepareUser() {

        UserEntity user = new UserEntity();
        user.setName(faker.name().name());
        user.setPassword(String.valueOf(faker.random().nextInt(1000000, 9999999)));
        user.setEmail(faker.bothify("?????????@gmail.com"));
        user.setLogin(faker.name().username());

        return user;
    }

    public static PhotoEntity preparePhoto() {

        PhotoEntity photo = new PhotoEntity();
        photo.setLink(faker.internet().image());
        photo.setName(faker.name().name());
        photo.setRating(faker.random().nextInt(0, 100));

        return photo;
    }

    public static CommentEntity prepareComment(UserEntity user, PhotoEntity photo) {
        CommentEntity comment = new CommentEntity();
        comment.setMessage(faker.letterify("?????? ?????? ????????? ??????????? ?????"));
        comment.setUser(user);
        comment.setPhoto(photo);
        return comment;
    }

    public static CollectionEntity prepareCollection(UserEntity user){
        CollectionEntity collection = new CollectionEntity();
        collection.setName(faker.name().name());
        collection.setDescription(faker.letterify("??? ????? ??????????? ?? ?????? ????? ?????"));
        collection.setUser(user);
        return collection;
    }


}
